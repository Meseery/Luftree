//
//  SearchDefaultInteractor.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/11/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

class LFSearchInteractor: SearchInteractor {
    private var remoteDataService: RemoteDataService?
    private var localDataService: LocalDataService?

    weak var presenter: SearchPresenter?

    init(remoteDataService: RemoteDataService,
         localDataService: LocalDataService) {
        self.remoteDataService = remoteDataService
        self.localDataService = localDataService
    }

    init() {
        self.remoteDataService = LFRemoteDataService(
            networkService: LFNetworkService.instance,
            tokenService: LFTokenService.instance)
        self.localDataService = LFLocalDataService.instance
    }

    func searchSchedules(searchParameters: LFSearchParameters,
                         onCompletion: @escaping (Result<[Schedule]?, AppError>) -> Void) {
        remoteDataService?.fetchSchedules(searchParams: searchParameters,
                                          onCompletion: onCompletion)
    }

    func searchAirports(_ keyword: String,
                        onCompletion: @escaping (Result<[LFAirport]?, AppError.LocalError>) -> Void) {
        guard let localDataService = localDataService as? LFLocalDataService else {
            onCompletion(.failure(AppError.LocalError.noDataFound))
            return
        }
        do {
            let airports = try localDataService.search(with: keyword)
            onCompletion(.success(airports))
        } catch let error as AppError.LocalError {
            onCompletion(.failure(error))
        } catch {}
    }
}
