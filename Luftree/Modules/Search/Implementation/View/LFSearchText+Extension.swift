//
//  LFSearchText+Extension.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import SearchTextField

extension SearchTextFieldItem {
    convenience init(_ item: SearchableItem) {
        self.init(title: item.itemTitle,
                  subtitle: item.itemSubTitle)
    }
}

extension SearchTextFieldItem: SearchableItem {
    var itemTitle: String {
        return self.title
    }
    var itemSubTitle: String? {
        return self.subtitle
    }
}

extension LFAirport: SearchableItem {
    var itemTitle: String {
        return self.iata
    }

    var itemSubTitle: String? {
        return self.name
    }
}
