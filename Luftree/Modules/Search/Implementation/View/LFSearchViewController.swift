//
//  SearchDefaultViewController.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/11/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import SearchTextField

class LFSearchViewController: UIViewController {
    @IBOutlet dynamic weak var originTextField: SearchTextField?
    @IBOutlet dynamic weak var destinationTextField: SearchTextField?
    @IBOutlet dynamic weak var datePicker: UIDatePicker?
    @IBOutlet dynamic weak var isDirectFlightSwitch: UISwitch?
    @IBOutlet dynamic weak var searchButton: UIButton?

    var presenter: SearchPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchTextFields()
        setupDatePicker()
        setupSearchButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func didSearch(_ sender: UIButton) {
        presenter?.didSearch(on: datePicker?.date ?? Date(), isDirectFlight: isDirectFlightSwitch?.isOn)
    }

    @IBAction func textFieldDidChange(_ textField: UITextField) {
        presenter?.didSearchAirports(with: textField.text)
    }
}

extension LFSearchViewController: SearchView {
    func reloadSearchSuggestions(with newSuggestions: [SearchableItem]) {
        let items = newSuggestions.compactMap { SearchTextFieldItem.init($0) }
        if let active = originTextField?.isEditing, active {
            originTextField?.filterItems(items)
        } else {
            destinationTextField?.filterItems(items)
        }
    }

    func showLoadingView() {
        searchButton?.startLoading()
    }

    func hideLoadingView() {
        searchButton?.stopLoading()
    }
}

extension LFSearchViewController {
    func setupSearchTextFields() {
        _ = [originTextField, destinationTextField].map {
            $0?.comparisonOptions = [.caseInsensitive]
            $0?.maxNumberOfResults = 20
            $0?.minCharactersNumberToStartFiltering = 2
            $0?.startSuggestingInmediately = true
            $0?.theme.bgColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            $0?.theme.separatorColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            $0?.theme.cellHeight = 50
            $0?.theme.font = UIFont(name: "Avenir", size: 14.0) ?? .systemFont(ofSize: 14.0)
        }
        originTextField?.itemSelectionHandler = { [weak self] items, idx in
            guard let `self` = self else { return }
            let item = items[idx]
            self.originTextField?.text = item.title
            self.presenter?.didSelectOrigin(item)
        }
        originTextField?.setLeftText("From:", padding: 5.0)
        destinationTextField?.itemSelectionHandler = { [weak self] items, idx in
            guard let `self` = self else { return }
            let item = items[idx]
            self.destinationTextField?.text = item.title
            self.presenter?.didSelectDestination(item)
        }
        destinationTextField?.setLeftText("To:", padding: 5.0)
    }

    func setupDatePicker() {
        datePicker?.minimumDate = Date()
        datePicker?.maximumDate = Date().addingTimeInterval(365*24*60*60)
    }

    func setupSearchButton() {
        searchButton?.layer.cornerRadius = 12.0
        searchButton?.backgroundColor = #colorLiteral(red: 0.9421710372, green: 0.44198066, blue: 0.2586857677, alpha: 1)
    }
}
