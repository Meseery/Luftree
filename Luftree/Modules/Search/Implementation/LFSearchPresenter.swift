//
//  SearchDefaultPresenter.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/11/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import SearchTextField

class LFSearchPresenter: SearchPresenter {
    var router: SearchRouter?
    var interactor: SearchInteractor?
    weak var view: SearchView?

    var originAirport: LFAirport?
    var destinationAirport: LFAirport?

    init(router: SearchRouter,
         interactor: SearchInteractor,
         view: SearchView) {
        self.router = router
        self.interactor = interactor
        self.view = view
    }

    var filteredAirports: [LFAirport]? {
        didSet {
            if let airports = filteredAirports {
                self.view?.reloadSearchSuggestions(with: airports)
            }
        }
    }

    deinit {
        router = nil
        interactor = nil
    }
}

// MARK: - Views Actions.
extension LFSearchPresenter {
    func didSelectOrigin(_ origin: SearchableItem) {
        originAirport = filteredAirports?.first {
            $0.iata == origin.itemTitle
        }
    }

    func didSelectDestination(_ destination: SearchableItem) {
        destinationAirport = filteredAirports?.first {
            $0.iata == destination.itemTitle
        }
    }

    func didSearch(on date: Date, isDirectFlight: Bool?) {
        guard let originAirport = originAirport,
            let destinationAirport = destinationAirport
            else {
                self.view?.showError(AppError.invalidInput)
                return
        }
        view?.showLoadingView()
        let searchParams = LFSearchParameters(
            originAirport,
            destinationAirport,
            date,
            isDirectFlight)
        interactor?.searchSchedules(searchParameters: searchParams,
                                    onCompletion: { [weak self] (result) in
                guard let `self` = self else { return }
                self.view?.hideLoadingView()
                if case let .success(schedules) = result {
                    guard let schedules = schedules else {
                        self.view?
                            .showError(AppError.invalidData)
                        return
                    }
                    self.router?
                        .navigateToSchedules(with: schedules)
                } else if case let .failure(error) = result {
                    self.view?.showError(error)
                }
        })
    }

    func didSearchAirports(with keyword: String?) {
        /// Validation Point
        guard let keyword = keyword,
            !keyword.isEmpty,
            keyword.count > 2 else {
                return
        }
        /// Interactor Point
        interactor?.searchAirports(keyword,
                                   onCompletion: { [weak self] (result) in
                                    guard let `self` = self else { return }
                                    if case let .success(airports) = result {
                                        self.filteredAirports = airports
                                    } else if case let .failure(error) = result {
                                        self
                                            .view?
                                            .showError(error)
                                    }
        })
    }
}
