//
//  SearchInteractor.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/11/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

protocol SearchInteractor {
    var presenter: SearchPresenter? { get set }
    func searchSchedules(searchParameters: LFSearchParameters,
                         onCompletion: @escaping (Result<[Schedule]?, AppError>) -> Void)
    func searchAirports(_ keyword: String,
                        onCompletion: @escaping (Result<[LFAirport]?, AppError.LocalError>) -> Void)
}
