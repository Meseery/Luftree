//
//  SearchViewController.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/11/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import SearchTextField

typealias SearchViewController = SearchView & UIViewController

protocol SearchView: class, ErrorViewLoadable {
    var presenter: SearchPresenter? { get set }
    func showLoadingView()
    func hideLoadingView()
    func reloadSearchSuggestions(with newSuggestions: [SearchableItem])
}
