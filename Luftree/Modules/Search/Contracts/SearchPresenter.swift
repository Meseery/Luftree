//
//  SearchPresenter.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/11/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

protocol SearchPresenter: class {
    var router: SearchRouter? { get set }
    var interactor: SearchInteractor? { get set }
    var view: SearchView? { get set }

    func didSearchAirports(with keyword: String?)
    func didSelectOrigin(_ origin: SearchableItem)
    func didSelectDestination(_ destination: SearchableItem)
    func didSearch(on date: Date, isDirectFlight: Bool?)
}
