//
//  LFRemoteDataService.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/10/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

protocol RemoteDataService {
    var networkService: NetworkServiceType? { get set }
    var tokenService: TokenService? { get set }
    func fetchSchedules(searchParams: LFSearchParameters,
                        onCompletion: @escaping CompletionClosure<[Schedule]?>)
}

typealias CompletionClosure<T> = (Result<T, AppError>) -> Void

class LFRemoteDataService: RemoteDataService {
    var networkService: NetworkServiceType?
    var tokenService: TokenService?

    init(networkService: NetworkServiceType,
         tokenService: TokenService) {
        self.networkService = networkService
        self.tokenService = tokenService
    }

    func fetchSchedules(searchParams: LFSearchParameters,
                        onCompletion: @escaping CompletionClosure<[Schedule]?>) {
        let searchEndPoint = LFEndpoint.search(searchParams)
        networkService?.extraHeaders = tokenService?.authHeader
        searchEndPoint
            .execute(networkService: networkService) { [weak self](result: Result<SchedulesResponse, AppError>) in
                guard let `self` = self else { return }
                switch result {
                case let .success(schedule):
                    onCompletion(
                        .success(
                            schedule
                                .scheduleResource?
                                .schedules))
                case let .failure(error):
                    self.handleError(error,
                                     endPoint: searchEndPoint,
                                     onCompletion: onCompletion)
                }
        }
    }

    func handleError<T: Codable>(_ error: AppError,
                                 endPoint: EndpointType,
                                 onCompletion: CompletionClosure<T>?) {
        if case .networkError(401) = error {
            networkService?.extraHeaders = nil
            tokenService?.getToken { [weak self] token in
                guard let `self` = self else { return }
                if token != nil {
                    self.networkService?.extraHeaders = self.tokenService?.authHeader
                    endPoint
                        .execute(networkService: self.networkService,
                                     onCompletion: onCompletion)
                } else {
                    onCompletion?(
                        .failure(AppError
                            .accessTokenUnavailable))
                }
            }
        } else {
            onCompletion?(.failure(error))
        }
    }
}
