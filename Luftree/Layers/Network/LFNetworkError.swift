//
//  LFNetworkError.swift
//  Luftree
//
//  Created by Mohamed EL Meseery on 3/11/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation

extension LocalizedError where Self: CustomStringConvertible {
    var errorDescription: String? {
        return description
    }
}

enum AppError: LocalizedError, CustomStringConvertible {
    case invalidInput
    case networkError(Int)
    case noInternetConection
    case invalidEndpoint
    case networkException
    case parsingError
    case invalidData
    case accessTokenUnavailable

    enum LocalError: LocalizedError, CustomStringConvertible {
        case invalidSearchKeyword
        case noDataFound

        var description: String {
            switch self {
            case .invalidSearchKeyword:
                return "Please check your search parameters"
            case .noDataFound:
                return "No data found matches your search parameters"
            }
        }
    }

    var description: String {
        switch self {
        case .invalidInput:
            return "Please check your search parameters"
        case .noInternetConection:
            return "Please check your internet connection"
        case .invalidEndpoint, .networkException, .parsingError:
            return "Something went wrong!"
        case .invalidData:
            return "Unable to fetch results!"
        case .accessTokenUnavailable:
            return "Something went wrong, please try again shortly!"
        case let .networkError(errorCode):
            switch errorCode {
            case 404:
                return "Sorry, couldn't find results matching your query"
            default:
                return "Sorry! unable to serve you this time"
            }
        }
    }
}
