//
//  LFSearchInteractorSpec.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Quick
import Nimble
@testable import Luftree

class LFSearchInteractorSpec: QuickSpec {
// swiftlint:disable function_body_length
    override func spec() {
        let remotedDataServiceMock = LFRemoteDataServiceMock(
            networkService: LFNetworkServiceMock(true,
                                                 stubFilename: "Schedules"),
            tokenService: LFTokenServiceMock())
        let localDataServiceMock = LFLocalDataService()
        let searchParamsMock = LFSearchParamMock.searchParameters
        let sut = LFSearchInteractor(
            remoteDataService: remotedDataServiceMock,
            localDataService: localDataServiceMock)

        context("[UT]: Testing Search Interactor") {
            describe("If tried to search airports with invalid keyword") {
                it("Should return invalid search keyword error") {
                    waitUntil { done in
                        sut.searchAirports("",
                                           onCompletion: { (result) in
                                            if case let .failure(error) = result {
                                                expect(error)
                                                    .to(matchError(AppError.LocalError.invalidSearchKeyword))
                                                done()
                                            } else {
                                                fail("Should return error")
                                            }

                        })
                    }
                }
            }

            describe("If tried to search airports with valid keyword") {
                it("Should return results or no data found") {
                    waitUntil { done in
                        sut.searchAirports("Ber",
                                           onCompletion: { (result) in
                                            if case let .success(airports) = result {
                                                expect(airports).notTo(beNil())
                                            } else if case let .failure(error) = result {
                                                expect(error)
                                                    .to(matchError(AppError.LocalError.noDataFound))
                                            }
                                            done()
                        })
                    }
                }
            }

            describe("If tried to search schedules, on failure") {
                remotedDataServiceMock.isDataFailureEnabled = true
                it("Should return error") {
                    waitUntil { done in
                        sut.searchSchedules(searchParameters: searchParamsMock,
                                           onCompletion: { (result) in
                                            if case let .failure(error) = result {
                                                expect(error)
                                                    .to(matchError(AppError.networkError(404)))
                                                done()
                                            } else {
                                                fail("Should return error")
                                            }

                        })
                    }
                }
            }

            describe("If tried to search schedules, on success") {
                it("Should return schedules results") {
                    remotedDataServiceMock.isDataFailureEnabled = false
                    waitUntil { done in
                        sut.searchSchedules(searchParameters: searchParamsMock, onCompletion: { (result) in
                            if case let .success(schedules) = result {
                                expect(schedules)
                                    .notTo(beNil())
                                done()
                            } else {
                                fail("Should return schedules")
                            }
                        })
                    }
                }
            }
        }
    }
    // swiftlint:enable function_body_length
}
