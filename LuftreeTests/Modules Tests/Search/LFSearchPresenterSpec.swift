import Quick
import Nimble
@testable import Luftree

class LFSearchPresenterSpec: QuickSpec {
    override func spec() {
        let routerMock = LFSearchRouterMock()
        let interactorMock = LFSearchInteractorMock()
        let viewMock = LFSearchViewMock()
        let sut = LFSearchPresenter(router: routerMock,
                                    interactor: interactorMock,
                                    view: viewMock)

        context("[UT]: Testing Search Presenter") {
            describe("If no origin/destination specified") {
                it("Should present invalid input error") {
                    sut.originAirport = nil
                    sut.destinationAirport = nil
                    sut.didSearch(on: Date(), isDirectFlight: false)
                    expect(viewMock.isShowingError).to(beTrue())
                    expect(viewMock.error)
                        .to(matchError(AppError.invalidInput))
                }
            }

            describe("[UT]: If interactor failed to load data") {
                it("Should present network error") {
                    sut.originAirport = LFSearchParamMock.searchParameters.origin
                    sut.destinationAirport = LFSearchParamMock.searchParameters.destination
                    interactorMock.isFailedToLoadData = true
                    sut.didSearch(on: Date(), isDirectFlight: false)
                    expect(viewMock.isShowingError).to(beTrue())
                }
            }

            describe("[UT]: If interactor successfully loaded data") {
                it("Should present results") {
                    interactorMock.isFailedToLoadData = false
                    sut.didSearch(on: Date(), isDirectFlight: false)
                    expect(routerMock.isNavigatedToSchedules)
                        .to(beTrue())
                }
            }
        }
    }
}
