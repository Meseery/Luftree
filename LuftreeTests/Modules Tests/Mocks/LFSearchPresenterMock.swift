//
//  LFSearchPresenterMock.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation
@testable import Luftree

class LFSearchPresenterMock: SearchPresenter {
    var router: SearchRouter?
    var interactor: SearchInteractor?
    var view: SearchView?
    var isSearchedAirports = false
    var isOriginSelected = false
    var isDestinationSelected = false
    var isSearchedSchedules = false

    func didSearchAirports(with keyword: String?) {
        isSearchedAirports = true
    }

    func didSelectOrigin(_ origin: SearchableItem) {
        isOriginSelected = true
    }

    func didSelectDestination(_ destination: SearchableItem) {
        isDestinationSelected = true
    }

    func didSearch(on date: Date, isDirectFlight: Bool?) {
        isSearchedSchedules = true
    }

    func reset() {
        isSearchedAirports = false
        isOriginSelected = false
        isDestinationSelected = false
        isSearchedSchedules = false
    }
}
