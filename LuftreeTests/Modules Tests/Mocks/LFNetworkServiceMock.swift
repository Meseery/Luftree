//
//  LFNetworkServiceMock.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation
@testable import Luftree

class LFNetworkServiceMock: NetworkServiceType {
    var extraHeaders: [String: String]?
    var isValidData: Bool
    var stubFileName: String

    init(_ isValidData: Bool, stubFilename: String) {
        self.isValidData = isValidData
        self.stubFileName = stubFilename
    }

    func getEntity<T: Codable>(forEndpoint endpoint: EndpointType,
                               onCompletion: @escaping (Result<T, AppError>) -> Void) {
        if isValidData {
            if let path = Bundle.init(for: type(of: self)).path(forResource: stubFileName, ofType: "json") ,
                let data = try? Data(contentsOf: URL.init(fileURLWithPath: path)),
                let entity = try? JSONDecoder().decode(T.self, from: data) {
                onCompletion(.success(entity))
            }
        } else {
            onCompletion(.failure(AppError.invalidData))
        }
    }

}
