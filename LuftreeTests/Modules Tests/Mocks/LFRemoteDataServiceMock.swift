//
//  LFRemoteDataServiceMock.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation
@testable import Luftree

class LFRemoteDataServiceMock: RemoteDataService {
    var networkService: NetworkServiceType?
    var tokenService: TokenService?
    var isDataFailureEnabled = false
    init(networkService: NetworkServiceType,
         tokenService: TokenService) {
        self.networkService = networkService
        self.tokenService = tokenService
    }

    func fetchSchedules(searchParams: LFSearchParameters,
                        onCompletion: @escaping CompletionClosure<[Schedule]?>) {
        if isDataFailureEnabled {
            onCompletion(.failure(AppError.networkError(404)))
        } else {
            let searchEndPoint = LFEndpoint.search(searchParams)
            searchEndPoint
                .execute(networkService: networkService) { (result: Result<SchedulesResponse, AppError>) in
                    switch result {
                    case let .success(schedule):
                        onCompletion(
                            .success(
                                schedule
                                    .scheduleResource?
                                    .schedules))
                    case let .failure(error):
                        onCompletion(.failure(error))
                    }
            }
        }
    }
}
