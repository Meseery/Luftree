//
//  LFSearchRouterMock.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import UIKit.UIViewController
@testable import Luftree

class LFSearchRouterMock: SearchRouter {
    var presenter: SearchPresenter?
    var view: SearchViewController?

    var isNavigatedToSchedules = false
    func navigateToSchedules(with schedules: [Schedule]) {
        isNavigatedToSchedules = true
    }
}
