//
//  LFSearchViewMock.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import SearchTextField
@testable import Luftree

class LFSearchViewMock: SearchViewController {
    var presenter: SearchPresenter?
    var isLoading = false
    var isShowingError = false
    var error: Error?

    func showLoadingView() {
        isLoading = true
    }

    func hideLoadingView() {
        isLoading = false
    }

    func showError(_ error: Error) {
        isShowingError = true
        self.error = error
    }

    func reloadSearchSuggestions(with newSuggestions: [SearchableItem]) {}
}
