//
//  LFRemoteDataServiceMock.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/13/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation
@testable import Luftree

class LFTokenServiceMock: TokenService {
    var accessToken: Token?

    var authHeader: [String: String]? {
        return ["Authorization": accessToken?.authToken ?? ""]
    }

    func getToken(_ onCompletion: ((Token?) -> Void)?) {
        let token = Token(accessToken: "",
                               tokenType: "Bearer",
                               expiry: nil)
        onCompletion?(token)
    }
}
