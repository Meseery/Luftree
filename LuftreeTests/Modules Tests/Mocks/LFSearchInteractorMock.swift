//
//  LFSearchInteractorMock.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation
@testable import Luftree

class LFSearchInteractorMock: SearchInteractor {
    var presenter: SearchPresenter?
    var isFailedToLoadData: Bool = false

    private var schedules: [Schedule]? {
        guard let path = Bundle.init(for: type(of: self)).path(forResource: "Schedules", ofType: ".json"),
            let data = try? Data(contentsOf: URL.init(fileURLWithPath: path)),
            let schedulesResponse = try? JSONDecoder().decode(SchedulesResponse.self, from: data)
        else { return nil }
        return schedulesResponse.scheduleResource?.schedules
    }

    func searchSchedules(searchParameters: LFSearchParameters,
                         onCompletion: @escaping (Result<[Schedule]?, AppError>) -> Void) {
        if isFailedToLoadData {
            onCompletion(.failure(AppError.networkError(404)))
        } else {
            onCompletion(.success(schedules))
        }
    }

    func searchAirports(_ keyword: String,
                        onCompletion: @escaping (Result<[LFAirport]?, AppError.LocalError>) -> Void) {}

}
