//
//  LFSearchParamMock.swift
//  LuftreeTests
//
//  Created by Mohamed EL Meseery on 3/14/19.
//  Copyright © 2019 Meseery. All rights reserved.
//

import Foundation
@testable import Luftree

struct LFSearchParamMock {
    static var searchParameters: LFSearchParameters {
        let origin = LFAirport.init(name: "Airport1", iata: "A1", city: "Any", lat: "Any", lon: "Any")
        let destination = LFAirport.init(name: "Airport2", iata: "A2", city: "Any", lat: "Any", lon: "Any")
        let date = Date()
        let isDirectFlight = false
        return LFSearchParameters(origin, destination, date, isDirectFlight)
    }
}
