import Quick
import Nimble
@testable import Luftree

class LFTokenServiceSpec: QuickSpec {
    override func spec() {
        let tokenService = LFTokenService.instance
        context("[UT]: Testing Remote Data Service") {
            describe("On getting new Token") {
                it("Should get valid one") {
                    waitUntil { done in
                        tokenService.getToken({ (token) in
                            expect(token).notTo(beNil())
                            done()
                        })
                    }
                }
            }
        }
    }
}
